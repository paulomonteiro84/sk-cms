<?php

/*****   FRONT   ******/

Route::get('/videos','ApplicationController@videos');

Route::get('/sounds','ApplicationController@sounds');

Route::get('/wallpapers','ApplicationController@wallpapers');


Route::get('/category/{id}','ApplicationController@showCategoryItems');

Route::get('/category/{name}/assets','ApplicationController@showCategoryAssets');

Route::get('/single/{id}','ApplicationController@showItems');


/*****   ADMIN   ******/
/***** Categories *****/


Route::get('/admin/categories', 'Admin\CategoriesController@index');

Route::get('/admin/categories/create', 'Admin\CategoriesController@create');

Route::post('/admin/categories/create', 'Admin\CategoriesController@store');

Route::delete('/admin/categories/delete/{id}', 'Admin\CategoriesController@destroy');

Route::get('/admin/categories/{name}', 'Admin\CategoriesController@edit');

Route::patch('/admin/categories/{id}', 'Admin\CategoriesController@update');

Route::get('/admin/categories/{name}/assets', 'Admin\AssetsController@category');

Route::post('/admin/categories/{name}/assets', 'Admin\AssetsController@createInCa');


/******* Assets ******/

Route::get('/admin', 'Admin\AssetsController@index');

Route::get('/admin/create', 'Admin\AssetsController@create');

Route::delete('/admin/delete/{id}', 'Admin\AssetsController@destroy');

Route::post('/admin/create', 'Admin\AssetsController@store');


/******** AJAX CALLS **********/


Route::get('/admin/tags', 'Admin\TagsController@search');





