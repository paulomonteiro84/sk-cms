<?php

namespace App\Http\Controllers\Admin;

use App\Asset;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class AssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
        $categories = Category::all();
        //Only completed tasks
        //$tasks= Task::where('completed',1)->get();

        return view('/admin/index', compact('assets','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        //Only completed tasks
        //$tasks= Task::where('completed',1)->get();

        return view('/admin/create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:assets|max:50',
            'category_id' => 'required'
        ]);

        $file_poster = '';
        $file_asset = '';
        $title = preg_replace('/\s+/', '', $request->title);
        $title = Str::lower($title);
        if(!empty($request->poster)){
            $file_poster = md5($title . time()). '.' . $request->poster->extension();
            $request->poster->storeAs('images', $file_poster);
        }
        if(!empty($request->asset)) {
            $file_asset = md5($title . time()) . '.' . $request->asset->extension();
            $path = $request->asset->storeAs('assets', $file_asset);
            if (strpos(strtolower($request->category), 'game') !== false) {
                $rootPath = config('filesystems.disks.local.root');
                $target = str_replace('.zip', '', $path);
                exec("unzip $rootPath/$path -d $rootPath/$target");
            }
        }
        Asset::create(
            [
                'title' => $request->title,
                'category_id' => $request->category_id,
                'poster' => $file_poster,
                'asset' => $file_asset,
                'promoted' => $request->promoted === 'on'
            ]
        );

        return redirect('/admin/categories/' . $request->category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $r = Asset::find($id);
        $r->delete();
        return redirect('/admin');
    }

    public function category($categoryName)
    {
        $category = Category::where('name', $categoryName)->get()->first();

        return view('/admin/create2', compact('category'));
    }
}
