<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $fillable = ['title','category_id','poster','asset','promoted'];

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

}
