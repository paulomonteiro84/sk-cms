@extends('admin.layouts.app')

@section('title','Admin add categorie')

@section('content')
    @include('admin.layouts/errors')
    <form action="/sk-cms/public/admin/categories/create" method="POST">
        {{ csrf_field() }}
        <div class="field">
            <label class="label">Name</label>
            <p class="control">
                <input class="input" name="name" type="text" placeholder="Text input">
            </p>
        </div>
        <div class="field is-grouped">
            <p class="control">
                <button class="button is-primary">Submit</button>
            </p>
        </div>
    </form>
@endsection

