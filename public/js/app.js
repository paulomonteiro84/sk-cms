$(document).ready(function () {
    swiperV = new Swiper('.swiper-container-v', {
        paginationClickable: true,
        direction: 'vertical',
        spaceBetween: 10,
    });
    swiperV.on('onSlideChangeEnd', function () {
        checkSlide(swiperV.activeIndex);
    });
});

var videos = document.getElementsByTagName("video");

function checkSlide(k){

    console.log(k);
    console.log(videos.length);

    if(k === videos.length-1){
        videos[k-1].pause();
        console.info('last item reached');
    }else if(k === 0){
        videos[k+1].pause();
    }else{
        videos[k-1].pause();
        videos[k+1].pause();
    }

   checkScroll(k);

}

function  checkScroll(k){
    videos[k].play();
    videos[k].addEventListener("ended", function () {
        setTimeout(function(){
            swiperV.slideTo(k+1,500, true);
            checkSlide(k+1)
        },1000);
    }, false);
}